<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m190930_113248_load_predefined_users
 */
class m190930_113248_load_predefined_users extends Migration
{
    /**
     * import predefined users data to database.
     */
    public function safeUp()
    {
        // Read JSON file
        $json = file_get_contents('users.json');

        // Decode JSON DATA
        $json_data = \yii\helpers\Json::decode($json);

        // Insert rows to DB
        foreach($json_data as $user_data) {
            $this->insert('user', [
                'id' => $user_data['id'],
                'first_name' => $user_data['first_name'],
                'last_name' => $user_data['last_name'],
                'email' => $user_data['email'],
                'personal_code' => $user_data['personal_code'],
                'phone' => $user_data['phone'],
                'active' => $user_data['active'],
                'dead' => $user_data['dead'],
                'lang' => $user_data['lang']
            ]);
        }
    }

    /**
     * Reverse action for import user data
     */
    public function safeDown()
    {
        // Empty table on revert
        $this->truncateTable('user');
    }
}
