<?php

use yii\db\Migration;
use yii\helpers\Json;

/**
 * Class m190930_120159_load_predefined_loans
 */
class m190930_120159_load_predefined_loans extends Migration
{
    /**
     * Importing predefined loans
     */
    public function safeUp()
    {
        // Read JSON file
        $json = file_get_contents('loans.json');

        // Decode JSON DATA
        $json_data = Json::decode($json);

        // Insert rows to DB
        foreach($json_data as $loan_data) {
            $this->insert('loan', [
                'id' => $loan_data['id'],
                'user_id' => $loan_data['user_id'],
                'amount' => $loan_data['amount'],
                'interest' => $loan_data['interest'],
                'duration' => $loan_data['duration'],
                'start_date' => date('Y-m-d', $loan_data['start_date']),
                'end_date' => date('Y-m-d', $loan_data['end_date']),
                'campaign' => $loan_data['campaign'],
                'status' => $loan_data['status']
            ]);
        }
    }

    /**
     * Empty table on revert
     */
    public function safeDown()
    {
        // Empty table on revert
        $this->truncateTable('loan');
    }
}
