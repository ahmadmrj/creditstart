<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'tester@example.com',
    'senderName' => 'tester',
    // Krajee extensions settings
    'bsVersion' => '3.x',
    'bsDependencyEnabled' => false
];
