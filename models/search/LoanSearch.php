<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Loan;

/**
 * LoanSearch represents the model behind the search form of `app\models\Loan`.
 */
class LoanSearch extends Loan
{
    /**
     * validation rules of fields
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'duration', 'campaign'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['start_date', 'end_date', 'user.first_name', 'user.last_name'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * different scenarios to get data from model
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'user.first_name',
            'user.last_name'
        ]);
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // use eager loading to get users related to each loan record
        $query = LoanSearch::find()
            ->joinWith('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'amount' => $this->amount,
            'interest' => $this->interest,
            'duration' => $this->duration,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'campaign' => $this->campaign,
            'status' => $this->status,
        ]);

        // filter user's first name and last name with containing characters.
        $query->andFilterWhere(['like','user.first_name', $this->getAttribute('user.first_name')]);
        $query->andFilterWhere(['like','user.last_name', $this->getAttribute('user.last_name')]);

        return $dataProvider;
    }
}
