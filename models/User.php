<?php

namespace app\models;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool $active
 * @property bool $dead
 * @property string $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * shows the table name in db
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * validation rules of fields
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['phone', 'personal_code'], 'number'],
            [['email'], 'email'], // Checks if email is valid address
            [['personal_code'], 'string', 'length' => [11, 11]], // Validate length of personal code after getting sure its numeric
            [['phone'], 'string', 'length' => [8, 8]],
            [['active', 'dead'], 'boolean'],
        ];
    }

    /**
     * shows the labels used for presenting each field of data
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang',
        ];
    }

    /**
     * defines the relation between loan and user model
     * @return \yii\db\ActiveQuery
     */
    public function getLoans(){
        return $this->hasMany(Loan::class, ['user_id' => 'id']);
    }

    /**
     * Calculates age by getting now diff with birth year.
     * @return int
     * @throws \Exception
     */
    public function getAge(){
        $birthDate = \DateTime::createFromFormat( 'Y', $this->getBirthYear());
        $now = new \DateTime();

        return $now->diff($birthDate)->y;
    }

    /**
     * Calculates the birth year by adding two digits of personal code (year) with century
     * @return string
     */
    private function getBirthYear(){
        $year = substr($this->personal_code, 1, 2) + $this->getBirthCentury();

        $birthDate = $year;

        return $birthDate;
    }

    /**
     * Calculates the birth century according to first digit of personal code
     * @return float|int
     */
    private function getBirthCentury(){
        $firstNo = substr($this->personal_code, 0, 1);
        return 1700 + ceil($firstNo / 2) * 100;
    }
}
