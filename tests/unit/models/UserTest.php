<?php
namespace tests\models;
use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * tests if user model return true result of converted data.
     * @return array|\yii\db\ActiveRecord|null
     */
    public function testFindUserById()
    {
        $user = User::find()
            ->where(['id' => 7521])
            ->one();

        $this->assertEquals($user->personal_code, '49005025465');

        return $user;
    }

    /**
     * tests if user "getAge()" method calculate correct age
     * @depends testFindUserById
     * @param User $user
     * @throws \Exception
     */
    public function testUserAgeCalculation(User $user)
    {
        $calculatedAge = $user->getAge();

        $this->assertEquals($calculatedAge, 29);
    }

    /**
     * gets a user from DB and check if is not under age to apply for a loan
     */
    public function testUserIsEligibleForLoanApply(){
        $user = User::find()->one();

        $this->assertGreaterThanOrEqual(18, $user->getAge());
    }

}
